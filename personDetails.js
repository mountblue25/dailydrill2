let array = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

//problem1

let result1 = array.filter((object) => {
    return object["gender"] === "Agender"
})
console.log(result1)

//problem2

let result2 = array.map((object) => {
    object["components"] = object["ip_address"].split(".")
    return object
})
console.log(result2)

//problem3

let result3 = array.reduce((accumelator, current) => {
    return accumelator += parseInt(current["ip_address"].split(".")[1])
}, 0);
console.log(result3)

//problem4

let result4 = array.reduce((accumelator, current) => {
    return accumelator += parseInt(current["ip_address"].split(".")[3])
}, 0);
console.log(result4)

//problem5

let result5 = array.map((object) => {
    object["full_name"] = object["first_name"] + " " + object["last_name"]
    return object
})
console.log(result5)

//problem6

let output = array.filter((object) => {
    return object["email"].endsWith(".org")
})

let result6 = output.map((object) => {
    return object["email"]
})
console.log(result6)

//problem7

let result7 = array.filter((object) => {
    if (object["email"].endsWith(".org") || object["email"].endsWith(".com") || object["email"].endsWith(".au")){
        return true
    }
})
console.log(result7.length)

//problem8

array.sort((object1, object2) => {
    const nameA = object1.first_name.toUpperCase();
    const nameB = object2.first_name.toUpperCase()
    if (nameA > nameB) {
      return -1;
    }
    return 0;
  });
console.log(array);



